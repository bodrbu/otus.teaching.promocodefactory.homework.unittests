﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class LimitBuilder
    {
        public static SetPartnerPromoCodeLimitRequest CreateBaseLimitRequest()
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = new DateTime(),
                Limit = 0
            };
        }

        public static SetPartnerPromoCodeLimitRequest WithActualDate(this SetPartnerPromoCodeLimitRequest limit)
        {
            limit.EndDate = new DateTime(2050, 11, 11);
            limit.Limit = 1;
            return limit;
        }

        public static SetPartnerPromoCodeLimitRequest WithLimitLessThanZero(this SetPartnerPromoCodeLimitRequest limit)
        {
            limit.Limit = -20;
            return limit;
        }
    }
}
